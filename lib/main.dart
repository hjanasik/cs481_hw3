import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Homework 3',
      theme: ThemeData(
        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Homework 3'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  List<Color> colorArray = [Colors.red, Colors.blue, Colors.orange, Colors.green, Colors.yellow];
  int randomIndexMinus = 0;
  int randomIndexPlus = 2;
  Timer timerImage;
  Timer timerCounter;
  Color textColor;
  List<String> imageString = ["friends.jpg", "himym.jpg", "la_casa_de_papel.jpg",
                              "murder.png", "peaky_blinders.jpg", "prison_break.jpg", "the_100.jpg"];
  int imageStringIndex = 0;
  int _startCounting = 10;
  bool favorite = false;

  @override void initState() {
    super.initState();
    // Timer _startCounter
    timerCounter = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_startCounting == 0)
          _startCounting = 10;
        else
          _startCounting--;
      });
    });
    // Timer swap image
    timerImage = Timer.periodic(Duration(seconds: 11), (Timer timer) {
      setState(() {
        if (imageStringIndex == imageString.length - 1)
          imageStringIndex = 0;
        else {
          imageStringIndex++;
        }
        //textColor = Color((Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0);
      });
    });
  }

  void _changeColorRandomlyMinus() {
    setState(() { Random randomtmp = Random(); randomIndexMinus = randomtmp.nextInt(colorArray.length); });
  }

  void _changeColorRandomlyPlus() {
    setState(() { Random randomtmp = Random(); randomIndexPlus = randomtmp.nextInt(colorArray.length); });
  }

  void _incrementCounter(int nb) {
    setState(() {
      if (_counter + nb < 0)
        _counter = 0;
      else
        _counter = _counter + nb;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          SizedBox(height: 20,),
          imageSwapping(),
          Center(child: Column(children: [
            Text("Here is a list of series that I like"),
            Text("Next serie in $_startCounting", style: TextStyle(fontWeight: FontWeight.bold),),
          ],),),
          //Text("TEST", style: TextStyle(color: textColor),),
          SizedBox(height: 50,),
          Center(child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              minusButton(),
              Text( '$_counter', style: Theme.of(context).textTheme.headline4,),
              plusButton(),
            ],
          )),
          SizedBox(height: 50,),
          favoriteButton(),
        ],
      ),
    );
  }
  Widget minusButton() {
    return RaisedButton(
      onPressed: () {
        _incrementCounter(-1);
        _changeColorRandomlyMinus();
      },
      child: Icon(Icons.minimize),
      shape: CircleBorder(),
      color : colorArray[randomIndexMinus],
    );
  }

  Widget plusButton() {
    return RaisedButton(
      onPressed: () {
        _incrementCounter(1);
        _changeColorRandomlyPlus();
      },
      child: Icon(Icons.add),
      shape: CircleBorder(),
      color: colorArray[randomIndexPlus],
    );
  }

  Widget imageSwapping() {
    return Container(
      child: Image.asset("assets/${imageString[imageStringIndex]}", height: 200, width: 200,),
    );
  }

  Widget favoriteButton() {
    return IconButton(
      icon: favorite == false ? Icon(Icons.favorite_border) : Icon(Icons.favorite),
      color: Colors.pink,
      onPressed: () {
        setState(() {
          if (favorite == true) 
            favorite = false;
          else
            favorite = true;
        });
      },
    );
  }
}
